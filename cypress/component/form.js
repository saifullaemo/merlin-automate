Cypress.Commands.add('formData', () => {
    // Assuming you have an array of data containing information about each field.
// The data array should have properties like fieldType, fieldName, validationCriteria, minValue, and maxValue.

const data = [
    { fieldType: "Text", fieldName: "Name", validationCriteria: "Length", minValue: 3, maxValue: 20 },
    { fieldType: "Whole Number", fieldName: "Age", validationCriteria: "Between", minValue: 18, maxValue: 65 },
    { fieldType: "Whole Number", fieldName: "Salary", validationCriteria: "Between", minValue: 1000, maxValue: 10000 },
    { fieldType: "Date", fieldName: "Date of Birth", validationCriteria: "Between", minValue: "01/01/1970", maxValue: "12/31/2024" },
    { fieldType: "Text", fieldName: "Department", validationCriteria: "List Items", listItems: "HR, Finance, Management" },
    { fieldType: "Text", fieldName: "Designation", validationCriteria: "Length", minValue: 3, maxValue: 20 },
    { fieldType: "Date", fieldName: "Joining Date", validationCriteria: "Between", minValue: "01/01/2000", maxValue: "12/31/2024" },
  ];
  
  // Function to generate a unique name for each field.
  const generateFieldName = (fieldName) => {
    return `field-${fieldName}-${Math.random().toString(36).substring(7)}`;
  };
  
  // Function to create a form field based on the provided data.
  const createFormField = (data, index) => {
    // Generate a unique name for the field.
    const fieldName = generateFieldName(data.fieldName);
    
    // Column Name selected dynamically using index
    let columnSelector;
    if (index === 0) {
      columnSelector = '#Column\\ Name'; // Use specific ID for the first element
    } else {
      // Construct dynamic selector for subsequent elements
      columnSelector = `:nth-child(${index + 4}) > .mt-20 > .gap-16 > .altd-text-field > .relative > #Column\\ Name`;
    }
    cy.get(columnSelector).type(data.fieldName);


    // Data type dd Select
    let dropdownButtonSelector;
    let dropdownFilterSelector;
  
    if (index === 0) {
      dropdownButtonSelector = 'div.altd-dropdown-single:nth-child(3) > button:nth-child(2)'
      dropdownFilterSelector = 'div.altd-dropdown-single:nth-child(3) > div:nth-child(3)';
    } else {
      dropdownButtonSelector = `div.relative:nth-child(${index + 4}) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > button:nth-child(2)`;
      dropdownFilterSelector = `div.relative:nth-child(${index + 4}) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div:nth-child(3)`;
    }
  
    cy.get(dropdownButtonSelector).click();
    cy.get(dropdownFilterSelector).contains(data.fieldType).click();
       
    // validation type dd Select
    let dropButtonSelector;
    let dropFilterSelector;

    if (index === 0) {
      dropButtonSelector = 'div.col-span-2:nth-child(4) > div:nth-child(1) > button:nth-child(2)';
      dropFilterSelector = 'div.col-span-2:nth-child(4) > div:nth-child(1) > div:nth-child(3)';
    } else {
      dropButtonSelector = `div.relative:nth-child(${index + 4}) > div:nth-child(2) > div:nth-child(1) > div:nth-child(4) > div:nth-child(1) > button:nth-child(2)`;
      dropFilterSelector = `div.relative:nth-child(${index + 4}) > div:nth-child(2) > div:nth-child(1) > div:nth-child(4) > div:nth-child(1) > div:nth-child(3)`;
    }
  
    cy.get(dropButtonSelector).click();
    cy.get(dropFilterSelector).contains(data.validationCriteria).click();

      // Set validation values  
      if (data.validationCriteria === 'Length') {
        let minValueSelector;
        let maxValueSelector;

        if (index === 0) {
          minValueSelector = '#Minimum';
          maxValueSelector = '#Maximum';
        } else {
          minValueSelector = `:nth-child(${index + 4}) > .mt-20 > .gap-16 > .gap-20 > .grid > :nth-child(1) > .relative > #Minimum`;
          maxValueSelector = `:nth-child(${index + 4}) > .mt-20 > .gap-16 > .gap-20 > .grid > :nth-child(2) > .relative > #Maximum`;
        }

        cy.get(minValueSelector).clear().type(data.minValue);
        cy.get(maxValueSelector).clear().type(data.maxValue);

      } else if (data.validationCriteria === 'List Items') {
        cy.get('#List\\ items').type(data.listItems);

      } else if (data.validationCriteria === 'Between') {
        let fromValueSelector;
        let toValueSelector;

        if (index === 0) {
          fromValueSelector = '#From';
          toValueSelector = '#To';
        } else {
          fromValueSelector = `:nth-child(${index + 4}) > .mt-20 > .gap-16 > .gap-20 > .grid > :nth-child(1) > .relative > #From`;
          toValueSelector = `:nth-child(${index + 4}) > .mt-20 > .gap-16 > .gap-20 > .grid > :nth-child(2) > .relative > #To`;
        }

          // Adjusting for 'Date' data type
        if (data.fieldType === 'Date') {
          fromValueSelector = ':nth-child(1) > .react-datetime-picker > .react-datetime-picker__wrapper > .react-datetime-picker__inputGroup';
          toValueSelector = ':nth-child(2) > .react-datetime-picker > .react-datetime-picker__wrapper > .react-datetime-picker__inputGroup';
        }
        cy.get(fromValueSelector).each(($el) => {
          cy.wrap($el).type(data.minValue);
        });
        cy.get(toValueSelector).each(($el) => {
          cy.wrap($el).type(data.maxValue);
        });
        
      }

    }
  
      // Iterate over the data array and create a form field for each item.
      data.forEach((item, index) => {
        console.log('Index:', index);
        cy.contains('.btn-link', "Create Column").click();
        createFormField(item, index);      
      });


});