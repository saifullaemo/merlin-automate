Cypress.Commands.add('Data', () => {
    cy.get('.pb-20 > .altd-dropdown-single > #dropdownSearchButton').click();
      cy.get('.pb-20 > .altd-dropdown-single > #ddFilter > .max-h-200 > :nth-child(3)').click();
      cy.url().should('eq', Cypress.config().baseUrl + 'data/table/list');
});

Cypress.Commands.add('Intervention', () => {
    cy.get('.pb-20 > .altd-dropdown-single > #dropdownSearchButton').click();
      cy.get('.pb-20 > .altd-dropdown-single > #ddFilter > .max-h-200 > :nth-child(2)').click();
      cy.url().should('eq', Cypress.config().baseUrl + 'intervention/list');
});

Cypress.Commands.add('Aggregation', () => {
    cy.get('.pb-20 > .altd-dropdown-single > #dropdownSearchButton').click();
      cy.get('.pb-20 > .altd-dropdown-single > #ddFilter > .max-h-200 > :nth-child(1)').click();
      cy.url().should('eq', Cypress.config().baseUrl + 'aggregation-vfm/dashboard');
});

Cypress.Commands.add('KnowledgeLibrary', () => {
    cy.get('.pb-20 > .altd-dropdown-single > #dropdownSearchButton').click();
      cy.get('.pb-20 > .altd-dropdown-single > #ddFilter > .max-h-200 > :nth-child(4)').click();
      cy.url().should('eq', Cypress.config().baseUrl + 'knowledge-library/files/list');
});

Cypress.Commands.add('settings', () => {
        cy.contains('.bottom > a.h-52','Settings').click();
    //  cy.get('.pb-20 > .altd-dropdown-single > #ddFilter > .max-h-200 > :nth-child(4)').click();
      cy.url().should('eq', Cypress.config().baseUrl + 'settings');
});
