import '../component/commands'

describe('Login Test', () => {
  beforeEach(() => {
    cy.login()  });

  it('should be authenticated across tests', () => {
      cy.url().should('eq', Cypress.config().baseUrl + 'aggregation-vfm/dashboard');
  });
});
