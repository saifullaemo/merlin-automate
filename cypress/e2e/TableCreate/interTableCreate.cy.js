import '../../component/commands'
import '../../component/sidebar'
import '../../component/form'

describe('Data Upload Test', () => {
  beforeEach(() => {
    // Reuse the existing login session
    cy.login();
    cy.wait(5000);
    // cy.session('loginSession_*');
    cy.url().should('eq', Cypress.config().baseUrl + 'aggregation-vfm/dashboard');
        
  });
  
  // Select a random option from a dropdown
  function selectRandomOption(selector) {
    cy.get(selector).then(($options) => {
      const randomIndex = Math.floor(Math.random() * $options.length);
      cy.wrap($options[randomIndex]).click();
    });
  }
 
  
  it('Table create workflow', () => {
      const randomString = Cypress._.random(0, 10);
      const tableBaseName = "Table";
      const tableName = `${tableBaseName} ${randomString}`;

      //Navigate to Data module
      cy.Data();

        // Click the New table button
        cy.contains('.btn-primary', "New Table").click();
        cy.wait(5000);
        cy.get('#Table\\ Name').type(tableName);

        
        // Select module
        cy.get('.right-column > :nth-child(1) > .altd-dropdown-single > #dropdownSearchButton').click();

        //select intervention module
        cy.get('.right-column > :nth-child(1) > .altd-dropdown-single > #ddFilter > .max-h-200 > :nth-child(1)').click();     
        cy.wait(3000);  
        
        //select market
        cy.get(':nth-child(2) > .altd-dropdown-single > #dropdownSearchButton').click();
        selectRandomOption(':nth-child(2) > .altd-dropdown-single > #ddFilter > .max-h-200');

        //select intervention
        cy.get(':nth-child(3) > .altd-dropdown-single > #dropdownSearchButton').click();
        selectRandomOption(':nth-child(3) > .altd-dropdown-single > #ddFilter > .max-h-200');

         //select frequency
         cy.get('.right-column > :nth-child(4) > .altd-dropdown-single > #dropdownSearchButton').click();
         selectRandomOption('.right-column > :nth-child(4) > .altd-dropdown-single > #ddFilter > .max-h-200');
         
         //select user
         cy.get('.altd-dropdown-multi > #dropdownSearchButton').click();
         selectRandomOption('.max-h-204 > :nth-child(1) > .pl-12');      
         
        //  cy.contains('.btn-link', "Create Column").click();      
         cy.formData();
         cy.contains('.btn-primary', "Create").click();      

         cy.Data();
         
         
      });
});