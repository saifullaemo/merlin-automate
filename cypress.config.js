const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: 'kyq98y',
  viewportWidth: 1440,
  viewportHeight: 768,

  e2e: {
    // experimentalRunAllSpecs: true,
    setupNodeEvents(on, config) {
      // implement node event listeners here
      
    },
    "baseUrl": "https://demodev.merlinapp.co.uk/"
  },
});
